declare module "@salesforce/apex/GetItems.getPageItems" {
  export default function getPageItems(param: {pageName: any}): Promise<any>;
}
